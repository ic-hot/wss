package ic.network.wss


import ic.network.wss.scope.ProxyWebSocketClientScope
import ic.network.wss.scope.WebSocketClientScope
import ic.util.log.logD

abstract class WebSocketClient : ProxyWebSocketClientScope {


	private var scope : WebSocketClientScope? = null

	override val sourceWebSocketClientScope get() = scope!!


	val isOpen get() = scope != null

	fun open (scope: WebSocketClientScope) {
		this.scope = scope
		notifyOpen()
	}

	fun notifyOpen() {
		logD("WebSocketClient") { "onOpen" }
		onOpen()
	}

	protected abstract fun onOpen()


	fun notifyTextReceived (text: String) {
		logD("WebSocketClient") { "onTextReceived\n$text" }
		onTextReceived(text)
	}

	protected open fun onTextReceived (text: String) {}


	fun notifyBinaryReceived (data: ByteArray) {
		onBinaryReceived(data)
	}

	protected open fun onBinaryReceived (data: ByteArray) {}


	fun notifyClose() {
		logD("WebSocketClient") { "onClose" }
		onClose()
		this.scope = null
	}

	protected abstract fun onClose()


}