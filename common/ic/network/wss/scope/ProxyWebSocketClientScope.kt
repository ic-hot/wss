package ic.network.wss.scope


interface ProxyWebSocketClientScope : WebSocketClientScope {


	val sourceWebSocketClientScope : WebSocketClientScope


	override fun sendText (text: String) {
		sourceWebSocketClientScope.sendText(text)
	}

	override fun sendBinary (data: ByteArray) {
		sourceWebSocketClientScope.sendBinary(data)
	}

	override fun closeSocket() {
		sourceWebSocketClientScope.closeSocket()
	}


}