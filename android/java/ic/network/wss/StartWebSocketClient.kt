package ic.network.wss


import java.net.URI
import java.net.URISyntaxException

import ic.base.throwables.UnableToParseException
import ic.base.throwables.ext.stackTraceAsString
import ic.ifaces.stoppable.Stoppable
import ic.network.wss.scope.WebSocketClientScope
import ic.struct.map.finite.FiniteMap
import ic.struct.map.finite.ext.foreach.forEach
import ic.util.log.logD
import ic.util.log.logI


fun startWebSocketClient (

	urlString : String,

	headers : FiniteMap<String, String> = FiniteMap(),

	toReconnectAutomatically : Boolean,
	
	client : WebSocketClient

) : Stoppable {

	val uri = (
		try {
			URI(urlString)
		} catch (_: URISyntaxException) {
			throw UnableToParseException.Runtime("urlString: $urlString")
		}
	)

	lateinit var wsClient : dev.gustavoavila.websocketclient.WebSocketClient

	val scope = object : WebSocketClientScope {

		override fun sendText (text: String) {
			wsClient.send(text)
		}

		override fun sendBinary (data: ByteArray) {
			wsClient.send(data)
		}

		override fun closeSocket() {
			wsClient.close()
			client.notifyClose()
		}

	}

	wsClient = object : dev.gustavoavila.websocketclient.WebSocketClient(uri) {

		override fun onOpen() {
			logD("startWebSocketClient") { "onOpen" }
			client.open(scope)
		}

		override fun onTextReceived (message: String) {
			logD("startWebSocketClient") { "onTextReceived $message" }
			client.notifyTextReceived(message)
		}

		override fun onBinaryReceived (data: ByteArray) {
			logD("startWebSocketClient") { "onBinaryReceived" }
			client.notifyBinaryReceived(data)
		}

		override fun onPingReceived (data: ByteArray?) {
			logD("startWebSocketClient") { "onPingReceived" }
			wsClient.sendPong(data)
		}

		override fun onPongReceived (data: ByteArray?) {
			logD("startWebSocketClient") { "onPongReceived" }
		}

		override fun onException (e: Exception) {
			logD("startWebSocketClient") { "onException ${ e.stackTraceAsString }" }
			client.notifyClose()
		}

		override fun onCloseReceived() {
			logD("startWebSocketClient") { "onCloseReceived" }
			client.closeSocket()
		}

	}

	wsClient.setConnectTimeout(16384)
	
	if (toReconnectAutomatically) {
		wsClient.enableAutomaticReconnection(1024)
	}

	headers.forEach { key, value ->
		wsClient.addHeader(key, value)
	}

	wsClient.connect()

	return Stoppable(
		stopNonBlockingOrThrowNotNeeded = {
			if (client.isOpen) {
				client.closeSocket()
			}
		},
		waitFor = {}
	)

}