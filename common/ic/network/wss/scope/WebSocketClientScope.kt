package ic.network.wss.scope


interface WebSocketClientScope {

	fun sendText (text: String)

	fun sendBinary (data: ByteArray)

	fun closeSocket()

}